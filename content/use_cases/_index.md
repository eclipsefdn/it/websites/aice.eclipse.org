---
title: "AICE Use Cases"
seo_title: "Use Cases - AICE"
keywords: ["AICE", "Use cases"]
---


## The AICE OpenLab

Status: Running.

The AICE OpenLab is a platform where partners can discuss and share AI-related resources, experiences and benchmarks.

It relies on [AI4EU Experiments](https://ai4europe.eu/) to provide a marketplace and visual editor to execute complex AI workflows, and also provides a Kubernetes cluster for the execution, demonstration and benchmarking of these workflows.

The first project to use the OpenLab platform is the AURA use case.

## The AURA Healthcare use case

Status: Complete.

> Check out the article we wrote at [The AURA demonstrator](/articles/aice_aura_demonstrator/)

We worked with the [AURA healthcare](https://aura.healthcare) association on an ML workflow that detects epileptic seizures before they happen, based on ECGs.

* Work with the team on the portability and industrialisation of the prototype.
* Bring the AURA ML workflow to the AICE OpenLab marketplace to foster exchange and collaboration on their solution.
* Provide a k8s cluster to execute resource-intensive tasks and demonstrate the workflow.

> See the [GitHub repository](https://github.com/borisbaldassari/aice-aura).

## Eclipse Graphene

Status: Running.

Eclipse Graphene provides an extensible marketplace of reusable solutions for AI and machine learning, sourced from a variety of AI toolkits and languages that ordinary developers, who are not machine-learning experts or data scientists, can easily use to create their own applications.

> [Eclipse Graphene](https://projects.eclipse.org/projects/technology.graphene)
