---
title: "Eclipse AI, Cloud & Edge (AICE) Working Group"
date: 2022-03-09T10:00:00+01:00
layout: "single"
footer_class: "footer-darker"
---

The Eclipse AI, Cloud & Edge (AICE) is an Eclipse Working Group, currently in construction, to promote the advancement, development and experimentation of open source software for AI, Cloud & Edge technologies.  It also manages and operates an open lab (the “AICE OpenLab”) that provides a set of resources to achieve these goals.

## Activities

The AICE WG and the associated AICE OpenLab achieve this by:

- Fostering open and neutral collaboration amongst members for the adoption of open source technologies.
- Defining, publishing and promoting reference architectures, blueprints and distributions of open source software that have been verified for industry AI, Cloud, and Edge standards, requirements, and use cases.
- Developing and providing open source verification test suites, test tools, calibrated datasets and hosted test infrastructure for industry AI, Cloud, and Edge standards, requirements and use cases.
- Ensuring that key requirements regarding privacy, security and ethics are integrated into all the OpenLab activities.
- Partnering with industry organizations to assemble and verify open source software for their standards, requirements, and use cases.
- Promoting the AICE OpenLab in the marketplace and engaging with the larger open source community.
- Managing and funding the lab infrastructure resources to support this work.

----

## Participating

### How to join?

Are you interested in joining AICE? It's easy, the working group is open to everyone.

First step to join us is to subscribe to the AICE mailing list that can be found on the [Eclipse mailing lists page](https://accounts.eclipse.org/mailing-list/aice-wg). Archives can be browsed [here](http://www.eclipse.org/lists/aice-wg).

If you would like to more actively engage with the initiative, please contact [Florent Zara](mailto:florent.zara-at-eclipse-foundation.org) or [Gaël Blondelle](mailto:gael.blondelle-at-eclipse-foundation.org).

### Monthly meetings

We also hold monthly meetings to discuss actions, organise events and monitor progress of our various tasks.

Meetings are announced on the mailing list.

----

## Supporters

[![AURA logo](images/partner-logos/aura-logo.png)](https://en.aura.healthcare/)

----

## Testimonial

> We worked with AICE to audit our current setup and improve the performance of our MLOps pipeline for epileptic seizure detection. We started from a working, although non-optimal, prototype and turned it into a fully optimised, reproducible and scalable workflow that can be seamlessly deployed almost anywhere.
>
> We now have a clean and well-structured repository that we can reuse for future developments, and a set of established best practices to help us build and deliver better software solutions.

- Alexis Comte, data scientist at AURA.

----
