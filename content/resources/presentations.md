---
title: "Presentations"
seo_title: "Presentations - AICE"
keywords: ["AICE"]
---

This section lists some material that was produced during our meetings, along with links and external references.

## AICE April 2022 Monthly Meeting (2022-04-07)

We were glad to welcome [AURA healthcare NPO](https://en.aura.healthcare/) that presented us the results of the [use case implemented using AICE](/articles/aice_aura_demonstrator/) OpenLab. We also provided the latest news on AICE.

Direct access to the replay:

* [Introduction to the AICE Working Group](https://www.youtube.com/watch?v=zXM0sHN0nKU&t=0s)
* [News of the AICE Working Group](https://www.youtube.com/watch?v=zXM0sHN0nKU&t=467s)
* [First AICE Use Case presentation with AURA Healthcare](https://www.youtube.com/watch?v=zXM0sHN0nKU&t=1275s)
* [Coming next on the AICE Working Group](https://www.youtube.com/watch?v=zXM0sHN0nKU&t=2650s)
* [Q&A session](https://www.youtube.com/watch?v=zXM0sHN0nKU&t=2807s)

You may also [download the slides](https://drive.google.com/file/d/1arYqtRiHmA5WNA-51mMcGPkD2rYksyGD/view?usp=sharing).

## AICE Meetup (2021-11-24)

This AICE OpenLab meetup was the first opportunity we had to join together after COVID to build the community around pilots and testbeds for AI, Cloud and Edge Computing. As the topic was also of interest to people who could not travel yet, we also offered the option to join online.

The meeting was held in Brussels, Belgium, at the Huawei office, 180 Chaussée d'Etterbeek, 1040 Etterbeek.

* Welcome speech and agenda -- Gaël Blondelle, Vice President, Ecosystem Development (Eclipse Foundation) \
  [Download slides](https://docs.google.com/presentation/d/13bY9lRCBVrBq8uuSKTjfkmH1SqbXy3Wi/edit?usp=sharing&ouid=101921549416138425666&rtpof=true&sd=true)
  
* EC OSS Study and AI Policy -- Paula Grzegorzewska, Senior Policy Advisor (OpenForum Europe) \
  [Download slides](https://drive.google.com/file/d/1ekBZOK_bjbVZA4NCo19kbEtffvNqTEWJ/view?usp=sharing) - [See video](https://youtu.be/tgSR7qZwifM)

* Gaia-X and AI -- Pierre Gronlier, Chief Technology Officer (Gaia-X) \
  [See video](https://youtu.be/raFQ5mW28Nw)

* The AICE OpenLab + Pilot -- Gaël Blondelle, Vice President, Ecosystem Development & Boris Baldassari (Eclipse Foundation) \
  [Download slides](https://drive.google.com/file/d/1bHlRx59LY4Y3UF0IO90Kacb0_jqLSwp1/view?usp=sharing)

* Beyond AIOps - How to "open source" operations and create free data -- Marcel Hild, Manager, AIOps, AI CoE CTO Office (RedHat) \
  [Download slides](https://drive.google.com/file/d/1bHlRx59LY4Y3UF0IO90Kacb0_jqLSwp1/view?usp=sharing) - [See video](https://youtu.be/DhA-halyroU)

* AI4EU Experiments -- Martin Welss, Senior Architect of AI4EU (Fraunhofer IAIS) \
  [Download slides](https://drive.google.com/file/d/1MLjTXHPgkUGcGtfgKG4SGPMmwEzssCkd/view?usp=sharing) - [See video](https://youtu.be/hmQld8Rep3A)

* AIPlan4EU -- Andrea Micheli, Post-Doctoral Researcher (Fundazione Bruno Kessler) \
  [Download slides](https://drive.google.com/file/d/1Llv4PSyirjr6KdRID0znV3tcrVZ9_6H3/view?usp=sharing) - [See video](https://youtu.be/dvPQcWuNZU0)

* MindSpore -- Jean-Baptiste Onofre, Open Source Specialist (Huawei) \
  [Download slides](https://drive.google.com/file/d/1uZYeI7nsvMJcnAG4x4oo_zi0FRL0aKzF/view?usp=sharing) - [See video](https://youtu.be/88yJceeklfg)

* Solid - secure decentralised data storage -- Philip Leroux, IOF Innovation Officer AI (IMEC) \
 [Download slides](https://drive.google.com/file/d/1PHLHLRck3y98OcaM5a9_fWwcY6kFlpXj/view?usp=sharing)

* Open Services Cloud -- Bryan Che, Chief Strategy Officer (Huawei) \
  [Download slides](https://drive.google.com/file/d/1oZOIi6YNaxIF9jQwl_AHOiBP-RsbnuE-/view?usp=sharing) - [See video](https://youtu.be/hrsmnPEBTL8)

* Open Euler -- Mauro Carvalho Chehab, Operating Systems Senior Engineer, Roberto Sassu, Senior Security Engineer Trusted Computing (Huawei) \
  [Download slides](https://drive.google.com/file/d/1Ox16B-ScMX8-2N6yZo9dr_H_Qsc8Cuvd/view?usp=sharing) - [See video](https://www.youtube.com/watch?v=5M0SZYJ7t-g)

* The brain needs a nervous system - Supporting Cloud to Thing AI -- Luca Cominardi, PhD, Senior Technologist (AdLink) \
  [Download slides](https://drive.google.com/file/d/17WQza9ryVjEm3tmcwjN0JcmhyBEEru8s/view?usp=sharing) - [See video](https://youtu.be/CkoC_KfdGqM)

* Conclusion & Wrap-up - Join the Working Group! -- Gaël Blondelle, Vice President, Ecosystem Development (Eclipse Foundation) \
  [See video](https://drive.google.com/file/d/1k-EcAfmBgAioF3SmreKwBFmN2d9jLxLh/view?usp=sharing)

## Eclipse Open Source AI Workshop S1E2 (2020-09-30)

* Video: [Welcome Message | Gaël Blondelle | Open Source AI Workshop S1E2](https://www.youtube.com/watch?v=cGQJ9BLichk&list=PLy7t4z5SYNaS5YMnhtOhZyLdMDxsQFNZz&index=7).
* Video: [Trustworthy AI & Open Source | Eclipse Open Source AI Workshop S1E2](https://www.youtube.com/watch?v=QU-0NvkVxHc&list=PLy7t4z5SYNaS5YMnhtOhZyLdMDxsQFNZz&index=8).
* Video: [Introduction to Pixano: an Open Source Tool to Assist Annotation of Image Databases | Open Source AI](https://www.youtube.com/watch?v=8W1l9TOHeu8&list=PLy7t4z5SYNaS5YMnhtOhZyLdMDxsQFNZz&index=9).

## Eclipse Open Source AI Workshop #1 (2020-06-11)

* Video: [Towards an open source AI initiative at the Eclipse Foundation](https://www.youtube.com/watch?v=7h2DT2Xn0No&list=PLy7t4z5SYNaS5YMnhtOhZyLdMDxsQFNZz).
* Video: [Political challenges and opportunities in making open source AI mainstream](https://www.youtube.com/watch?v=Isr3diSB30c&list=PLy7t4z5SYNaS5YMnhtOhZyLdMDxsQFNZz&index=3).
* Video: [Eclipse Deeplearning4j: How to run AI workloads on Jakarta EE compliant servers](https://www.youtube.com/watch?v=B3a7ceU3API&list=PLy7t4z5SYNaS5YMnhtOhZyLdMDxsQFNZz&index=4).
* Video: [Meet MindSpore, the new open source AI framework!](https://www.youtube.com/watch?v=pqgQaVOcVV0&list=PLy7t4z5SYNaS5YMnhtOhZyLdMDxsQFNZz&index=5)
* Video: [Questions and Answers](https://www.youtube.com/watch?v=ajhubu-LVBk&list=PLy7t4z5SYNaS5YMnhtOhZyLdMDxsQFNZz&index=6)
