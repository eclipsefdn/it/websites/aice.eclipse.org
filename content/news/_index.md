---
title: "News & Events"
seo_title: "News & Events - AICE"
date: 2022-03-08T10:00:00+01:00
keywords: ["AICE"]
---

{{< grid/section-container class="featured-news margin-top-40">}}
  {{< grid/div class="row" isMarkdown="false" >}}
    {{< grid/div class="col-sm-12" isMarkdown="false" >}}
      {{< newsroom/news
          title="News"
          id="news-list-container"
          publishTarget="aice"
          count="5"
          class="col-sm-24"
          templateId="custom-news-template"
          paginate="true" >}}
    {{</ grid/div >}}

    {{< grid/div class="col-sm-12 featured-events text-center" isMarkdown="false" >}}
      {{< newsroom/events
          title="Upcoming Events"
          publishTarget="aice"
          class="col-sm-24"
          containerClass="event-timeline"
          upcoming="1"
          templateId="custom-events-template" templatePath="js/templates/event-list-format.mustache"
          count="4"  includeList="true" >}}
    {{</ grid/div >}}
   {{</ grid/div >}}
{{</ grid/section-container >}}
